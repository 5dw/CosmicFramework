# CosmicFramework
## Implementation
### MVC
The MVC implemenation in CosmicFramework is based on 3 classes,
"Database", "Controller", and "View". The 3 classes work together,
with the goal of finally rendering a page for the user.

#### _Each objects function_
* Database (and models)
    
    The database is a simple object used to connect and query to a configured database.
    In CosmicFramework, to create a model, which implements this database into a class
    that grabs data, you create a class extending the database. You can then include this
    model inside of your controller to access your defined methods to grab data. 
* View
    
    The View contains the template to render, the renderer (plates), and data to pass
    into the template. It is standalone in function, however, the controller holds one
    for rendering.
* Controller
    
    The Controller contains the logic behind rendering a View object, such as running
    the included model's logic, authentication, etc. In it's usage, you would create a 
    class that extends Controller, and implement logic for the abstract "logic" method.
    This logic method is automatically ran upon calling Controller->render()

Example implementation

_test.php_
```php
    require_once __DIR__ . '/../vendor/autoload.php';

    use CosmicFramework\MVC\Database;

    Database::init("host", "database", "root", "");

    // when instantiating the controller class, you pass in the controllers view name and directory
    $test_controller = new TestController("test_view", __DIR__);

    // render() first runs the controllers logic() method, then renders the view
    $test_controller->render();
```

_test_controller.php_
```php
    use CosmicFramework\MVC\Controller;

    class TestController extends Controller {
        // This logic is ran when calling the controller's render() method
        public function logic() {
            require(__DIR__ . "/test_model.php");
            $test_model = new TestModel();
            $data = $test_model->GetData();
            $this->view->setItems($data);
        }
    }
```

_test_model.php_
```php
    use CosmicFramework\MVC\Database;

    class TestModel extends Database {
        public function GetData() {
            return self::query("SELECT * FROM data");
        }
    }

```

### API
## CosmicAccounts
CosmicAccounts (CosmicFramework\API\CosmicAccounts), is a static helper class
for dealing with the CosmicAccounts API. It has 2 functions:

o AuthenticateUser($conf_key) - Takes a CosmicAccounts conf key, checks if it's valid, and returns the permissions linked to the key  
o AuthRedirect($public_key) - Redirects to the CosmicAccounts authentication redirect URL with the user's public_key