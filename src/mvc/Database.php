<?php

namespace CosmicFramework\MVC;
use PDO;

class Database {
    static $host;
    static $database;
    static $username;
    static $password;

    public static function init($host, $database, $username, $password) {
        self::$host = $host;
        self::$database = $database;
        self::$username = $username;
        self::$password = $password;
    }

    private static function connect() {
        $pdo = new PDO('mysql:host=' . self::$host . ';dbname=' . self::$database . ';', self::$username, self::$password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    public static function query($query, $params = array()) {
        $statement = self::connect()->prepare($query);
        $statement->execute($params);

        if (explode(' ', $query)[0] == 'SELECT') {
            $data = $statement->fetchAll();
            return $data;
        }
    }
}